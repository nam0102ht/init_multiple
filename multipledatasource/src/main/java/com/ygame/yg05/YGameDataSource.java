/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ygame.yg05;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
// Load to Environment
// (@see resources/application.properties).
public class YGameDataSource {
    @Autowired
    Environment env;
    
    @Bean
    public DataSource dataSource(){
        DriverManagerDataSource dataSource =  new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name.ygame"));
        dataSource.setUrl(env.getProperty("spring.datasource.url.ygame"));
        dataSource.setUrl(env.getProperty("spring.datasource.username.ygame"));
        dataSource.setUrl(env.getProperty("spring.datasource.password.ygame"));
        return dataSource;
    }
    
    @Bean
    public LocalContainerEntityManagerFactoryBean ygameEntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());

        // Scan Entities in Package:
        em.setPackagesToScan(new String[] { ConstantsDatasource.PACKAGE_ENTITIES_YGAME });
        em.setPersistenceUnitName(ConstantsDatasource.JPA_UNIT_NAME_YGAME); // Important !!

        //
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

        em.setJpaVendorAdapter(vendorAdapter);

        HashMap<String, Object> properties = new HashMap<>();

        // JPA & Hibernate
        properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect.ygame"));
        properties.put("hibernate.show-sql", env.getProperty("spring.jpa.show-sql.ygame"));
        properties.put("hibernate.ddl-auto", env.getProperty("spring.jpa.hibernate.ddl-auto.ygame"));
        em.setJpaPropertyMap(properties);
        em.afterPropertiesSet();
        return em;
    }
    @Bean
    public PlatformTransactionManager ygameTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(ygameEntityManager().getObject());
        return transactionManager;
    }
}
